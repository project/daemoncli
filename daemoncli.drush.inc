<?php

/**
 * @file
 * CLI Daemon drush integration. 
 */

/**
 * Implementation of hook_drush_command().
 */
function daemoncli_drush_command() {
  $items = array();
  
  $items['daemon.run'] = array(
    'description' => 'Launch daemon',
    'callback'    => '_daemoncli_daemonize', 
  );
  $items['daemon.kill'] = array(
    'description' => 'Kill and cleanup state for daemon',
    'callback'    => '_daemoncli_kill',
  );
  
  return $items;
}

function _daemoncli_daemonize() {
  if (module_exists('daemoncli') == FALSE) {
    drush_print(dt("You need to activate the module first"));
    return; 
  }

  $daemon_pid = variable_get(DAEMONCLI_PID, 0);

  if ($daemon_pid) {
    drush_print(dt("Daemon already running"));
    return;
  }

  $daemon_path = escapeshellcmd(drupal_get_path('module', 'daemoncli') . "/daemoncli.php ");

  // Lauching process
  shell_exec("php -f {$daemon_path} > /dev/null");
}

function _daemoncli_kill() {
  $daemon_pid = variable_get(DAEMONCLI_PID, 0);

  // Check if daemon is running
  if (!$daemon_pid) {
    drush_print(dt("No running daemon"));
    return;
  }
  
  // Check if daemon pid is in memory
  if (daemoncli_is_running() == FALSE) {
    drush_print(dt("Daemon is not running, changing memory state"));
    variable_set(DAEMONCLI_PID, 0);
    variable_set(DAEMONCLI_LAST_EXECUTE, 0);
    return;
  } 
  
  // Sending the SIGTERM signal to daemon
  drush_print(dt("Sending SIGTERM signal to daemon"));
  posix_kill($daemon_pid, SIGTERM);
}
