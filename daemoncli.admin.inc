<?php

/**
 * @file
 * CLI Daemon administration pages.
 */

/**
 * Daemon configuration page wrapper.
 */
function daemoncli_build($form_state) {
  if (isset($form_state['operation'])) {
    switch ($form_state['operation']) {
      case 'kill':
        return _daemoncli_build_stop_confirm($form_state);
      case 'run':
        return _daemoncli_build_run_confirm($form_state);
    }
  }
  return _daemoncli_build($form_state);
}

/**
 * Real admin page.
 */
function _daemoncli_build($form_state) {
  $form = array();

  if ($running = daemoncli_is_running()) {
    $message = t("Daemon is running since %time.", array(
      '%time' => format_date(variable_get(DAEMONCLI_START, 0)),
    ));
    $message .= '<br/>' . t("Latest content update was done at %time.", array(
      '%time' => format_date(variable_get(DAEMONCLI_LAST_EXECUTE, 0)),
    ));
  }
  else {
    $message = t('<strong>Daemon is not running</strong>. Please run it manually.');
  }

  $form['text_daemon_status'] = array(
    '#type' => 'markup',
    '#prefix' => '<div>',
    '#suffix' => '</div>',
    '#value' => '<p>' . $message . '</p>',
  );

  $form['run_daemon'] = array(
    '#type' => 'submit',
    '#value' => t('Run daemon'),
    '#submit' => array('daemoncli_build_run_submit'),
    '#disabled' => $running,
  );

  $form['kill_daemon'] = array(
    '#type' => 'submit',
    '#value' => t('Kill daemon'),
    '#submit' => array('daemoncli_build_stop_submit'),
    '#disabled' => ! $running,
  );

  $account = daemoncli_get_user();

  $form['daemon_user'] = array(
    '#type' => 'textfield',
    '#title' => t("Drupal user which runs the daemon"),
    '#size' => 20,
    '#description' => t("You can write here a valid <strong>username</strong> or <strong>uid</strong>. If you leave empty, the super user will be user (known as user 1 in Drupal), which can be dangerous for your site security. Notice that the user must have the '%perm' permission. If user does not exists or do not have this permission, super user will be used.", array(
      '%perm' => 'administer site configuration',
    )),
    '#default_value' => $account->name,
  );

  $form[DAEMONCLI_CRON] = array(
    '#type' => 'checkbox',
    '#title' => t('Run cron when nothing else to do.'),
    '#description' => t('If the daemon has nothing to do, it will aptempt to run the cron instead.'),
    '#default_value' => variable_get(DAEMONCLI_CRON, TRUE),
  );
  $form[DAEMONCLI_CRON_DELAY] = array(
    '#type' => 'textfield',
    '#title' => t('Cron delay'),
    '#description' => t('Set a number of minutes between to cron run aptempt.'),
    '#default_value' => variable_get(DAEMONCLI_CRON_DELAY, 10),
  );
  $form[DAEMONCLI_UI_DEBUG_MODE] = array(
    '#type' => 'checkbox',
    '#title' => t("Debug mode"),
    '#description' => t("If checked, the deamon will be run in debug mode when run via this user interface."),
    '#default_value' => variable_get(DAEMONCLI_UI_DEBUG_MODE, 0),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#validate' => array('daemoncli_build_set_user_validate'),
    '#submit' => array('daemoncli_build_set_user_submit'),
  );

  return $form;
}

function daemoncli_build_set_user_validate($form, &$form_state) {
  $identifier = $form_state['values']['daemon_user'];
  $error = FALSE;

  if (empty($identifier)) {
    // Default value is ok, no processing to do here.
  }
  else if (is_numeric($identifier)) {
    $error = !($account = user_load(array('uid' => $identifier)));
  }
  else {
    $error = !($account = user_load(array('name' => (string) $identifier)));
  }

  if ($error) {
    form_set_error('daemon_user', t('This user does not exists'));
  }
  else if (isset($account)) {
    $form_state['values']['uid'] = $account->uid;
  }

  $delay = $form_state['values'][DAEMONCLI_CRON_DELAY];
  if (empty($delay) || !is_numeric($delay)) {
    form_set_error(DAEMONCLI_CRON_DELAY, t("Cron delay value must be a numeric value."));
  }
}

function daemoncli_build_set_user_submit($form, &$form_state) {
  $account = $form_state['values']['daemon_user'];
  if (isset($form_state['values']['uid'])) {
    variable_set('daemoncli_uid', $form_state['values']['uid']);
    drupal_set_message(t('Daemon user changed.'));
  }
  else {
    variable_set('daemoncli_uid', 0);
    drupal_set_message(t('Daemon user reset. Daemon will now uses the super user.'));
  }
  // Set wether daemoncli should run cron if there is nothing to do.
  variable_set(DAEMONCLI_UI_DEBUG_MODE, $form_state['values'][DAEMONCLI_UI_DEBUG_MODE]);
  variable_set(DAEMONCLI_CRON, $form_state['values'][DAEMONCLI_CRON]);
  variable_set(DAEMONCLI_CRON_DELAY, $form_state['values'][DAEMONCLI_CRON_DELAY]);
  drupal_set_message(t('Configuration options saved.'));
}

/**
 * Submit handler to run daemon.
 */
function daemoncli_build_run_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_state['operation'] = 'run';
}

function _daemoncli_build_run_confirm($form_state) {
  $form = array('#submit' => array('daemoncli_build_run_confirm_submit'));
  return confirm_form($form, t("Are you sure you wan't to run the daemon?"), 'admin/build/daemon', t('This will run the daemon process.'));
}

/**
 * Submit handler that really runs the daemon.
 */
function daemoncli_build_run_confirm_submit($form, &$form_state) {
  $command = "php -f " . drupal_get_path('module', 'daemoncli') . '/daemoncli.php';
  if (variable_get(DAEMONCLI_UI_DEBUG_MODE, 0)) {
    $command .= ' -- --debug';
  }
  $command = escapeshellcmd($command);
  shell_exec($command . ' > /dev/null');
  // Sleep is arbitratry, but as the daemon has a 500ms loop, 1s is sufficient
  // to be sure that the next page will see the daemon is running.
  sleep(1);
  drupal_set_message(t("Daemon is now running."));
}

/**
 * Submit handler to stop daemon from running.
 */
function daemoncli_build_stop_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_state['operation'] = 'kill';
}

function _daemoncli_build_stop_confirm($form_state) {
  $form = array('#submit' => array('daemoncli_build_stop_confirm_submit'));
  return confirm_form($form, t("Are you sure you wan't to kill the daemon?"), 'admin/build/daemon', t('This will kill nicely the daemon process and prevent it from running until you launch it manually.'));
}

/**
 * Submit handler that really shutdowns the daemon.
 */
function daemoncli_build_stop_confirm_submit($form, &$form_state) {
  variable_set(DAEMONCLI_STOP, TRUE);
  // Sleep is arbitratry, but as the daemon has a 500ms loop, 1s is sufficient
  // to be sure that the next page will see the daemon is killed.
  sleep(1);
  drupal_set_message(t('Send the kill signal to the daemon.'));
}

