<?php

/**
 * @file
 * CLI Daemon service.
 */

/**
 * Main thread business stuff. This method will attempt to find actions defined
 * by modules and run them.
 */
function children_process($savePid) {
  global $debug;

  // Create drupal environment
  children_drupal_environment();

  posix_setsid();

  if ($savePid == TRUE) {
    variable_set(DAEMONCLI_PID, posix_getppid());
    variable_set(DAEMONCLI_START, time());
  }

  $has_run = FALSE;

  // FIXME: this implementation is quite ugly.
  $modules = module_implements('daemoncli_run', TRUE);
  $count = count($modules);
  $last = variable_get(DAEMONCLI_LAST_MODULE, 0) + 1;
  if ($last >= $count) {
    $last = 0;
  }
  // Invoke the hook for only one module, which is the last one +1.
  // This algorithm ensures that every module has a chance to run something.
  for ($i = $last; $i < $count; $i++) {
    if (module_invoke($modules[$i], 'daemoncli_run')) {
      watchdog('daemoncli', "Ran job for module " . $modules[$i], NULL, WATCHDOG_INFO);
      // Ensure we run only one task.
      variable_set(DAEMONCLI_LAST_MODULE, $i);
      $has_run = TRUE;
      break;
    }
  }

  // Let a chance to job_queue module to do its job if anything happened.
  if (!$has_run && module_exists('job_queue')) {
    $job_count = db_result(db_query('SELECT count(*) FROM {job_queue}'));
    if ($job_count) {
      job_queue_cron();
      if ($debug) {
        watchdog('daemoncli', "Daemon ran job_queue cron", NULL, WATCHDOG_DEBUG);
      }
      $has_run = TRUE;
    }
  }

  // Else, we have to do something!
  if (!$has_run && variable_get(DAEMONCLI_CRON, TRUE)) {
    children_cron_run();
  }

  // Check if admin asks to kill daemon
  if (variable_get(DAEMONCLI_STOP, 0) == 1) {
    if ($debug) {
      watchdog('daemoncli', "Exit by user interface", NULL, WATCHDOG_DEBUG);
    }

    // Get parent pid
    $pid = variable_get(DAEMONCLI_PID, 0);

    if ($pid == 0) {
      watchdog("daemoncli", 'Unable to find pid', NULL, WATCHDOG_ERROR);
      return;
    }

    watchdog("daemoncli", 'Daemon stopped by user interface', NULL, WATCHDOG_INFO);
    variable_set(DAEMONCLI_STOP, 0);

    // Sending signal to parent
    posix_kill($pid, SIGTERM);
  }

  return;
}

/**
 * Run the Drupal cron.
 */
function children_cron_run() {
  global $debug;

  $last = variable_get('cron_last', 0);
  $delay = variable_get(DAEMONCLI_CRON_DELAY, 1);
  if ($delay < 1) {
    $delay = 10; 
  }

  if ($last + $delay * 60 < time()) {
    drupal_cron_run();
    if ($debug) {
      watchdog('daemoncli', "Ran cron", NULL, WATCHDOG_DEBUG);
    }
  }
}

/**
 * Set some Drupal environement contextual information, such as override the
 * current logged in user.
 */
function children_drupal_environment() {
  global $user, $debug;
  
  // Login as given user
  if (module_exists('daemoncli')) {
    $user = daemoncli_get_user();
  }
  else {
    // Failsafe option, daemon can run without its module.
    $user = user_load(1);
  }

  if ($debug) {
    watchdog('daemoncli', "Drupal child boostrap using '" . $user->name . "' user", NULL, WATCHDOG_DEBUG);
  }

  // No saved session
  session_save_session(FALSE);
}
