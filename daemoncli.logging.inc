<?php

/**
 * @file
 * CLI Daemon logging primitives.
 */

/**
 * Get current log file pointer.
 */
function daemoncli_log_open_file($shutdown = FALSE) {
  // This will store the current log file handle.
  global $daemon_cli_log_handle, $daemon_logging;

  if ($daemon_logging) {
    return FALSE;
  }

  // Shutdown close properly the file.
  if ($shutdown) {
    if ($daemon_cli_log_handle && $daemon_cli_log_handle != -1) {
      fwrite($daemon_cli_log_handle, _daemoncli_log_append_ts("Log session closed"));
      fclose($daemon_cli_log_handle);
      $daemon_cli_log_handle = FALSE;
    }
  }

  // Attempt to open file if not already tried.
  if (!$daemon_cli_log_handle) {
    // Get file path.
    $file = 'daemon.log';

    // Store error.
    $error = '';

    // Check file already exists.
    if (file_exists($file)) {

      // If a backup log exists, remove it.
      if (file_exists($file  . '.1')) {
        if (!@unlink($file)) {
          $error = "Unable to unlink the '" . $file . ".1' backup file";
        }
      }

      // Move the current file as the backup file.
      if (!@rename($file, $file . '.1')) {
        $error = "Unable to move the existing log as the '" . $file . ".1' backup file";
      }
    }

    if (($daemon_cli_log_handle = @fopen($file, 'a+')) === FALSE) {
      $handle = -1;
      $func = function_exists('watchdog') ? $watchdog : 'daemoncli_watchdog';
      $func('daemoncli', "Daemon was unable to create log file '" . $file . "', error was: " . ($error ? $error : 'Unable to open file for writing'), NULL, WATCHDOG_ERROR);
    }
    else {
      fwrite($daemon_cli_log_handle, _daemoncli_log_append_ts("Log session started"));
    }
  }

  return $daemon_cli_log_handle == -1 ? FALSE : $daemon_cli_log_handle;
}

function _daemoncli_log_append_ts($message) {
  global $daemon_pid;
  return date('c') . " (" . ($daemon_pid == -1 ? 'parent' : $daemon_pid) . ") " . $message . "\n";
}

/**
 * Write a message into current log.
 * 
 * Please do not use this method directly, even if you are in debug mode.
 * Deamon debug messages should get through a watchdog() call, the daemon
 * hook_watchdog() implementation will do its own job.
 */
function daemoncli_log_message($message) {
  global $debug;

  $message = _daemoncli_log_append_ts($message);

  // Debug mode, display message in console.
  if ($debug) {
    print $message;
  }

  // Whatever is the context, write the message into log file.
  if ($handle = daemoncli_log_open_file()) {
    fwrite($handle, $message);
  }
}

/**
 * Implementation of hook_watchdog().
 * 
 * By implementing this hook in this particular file, we ensure it will exists
 * only in CLI execution context.
 */
function daemoncli_watchdog($log_entry) {
  $prefix = '';

  switch ($log_entry['severity']) {
    case WATCHDOG_ALERT:
      $prefix = "ALERT";
      break;

    case WATCHDOG_CRITICAL:
      $prefix = "*** CRITICAL ***";
      break;

    case WATCHDOG_DEBUG:
      $prefix = "DEBUG";
      break;

    case WATCHDOG_EMERG:
      $prefix = "*** EMERGENCY, SYSTEM IS UNUSABLE ***";
      break;

    case WATCHDOG_ERROR:
      $prefix = "*** ERROR ***";
      break;

    case WATCHDOG_INFO:
      $prefix = "INFO";
      break;

    case WATCHDOG_NOTICE:
    default:
      $prefix = "NOTICE";
      break;
  }

  // Do some formatting.
  $message = "[" . $log_entry['type'] . "] " . $prefix . " ";
  if (is_array($log_entry['variables'])) {
    $message .= strtr($log_entry['message'], $log_entry['variables']);
  }
  else {
    $message .= $log_entry['message'];
  }

  daemoncli_log_message($message);
}
