<?php

/**
 * CLI Daemon hooks definition.
 */

/**
 * Use the daemon to do something. Remember that other modules can have to
 * do something too, so please avoid to always do something and let the
 * chance for other modules to do something.
 * 
 * Do whatever you have to do here. You will have access to a full Drupal
 * bootstrapped environment, just like cron will do.
 * 
 * @return boolean
 *   TRUE if your module did something.
 */
function hook_daemoncli_run() {
  if ($i_have_to_do_something) {
    do_something();
    return TRUE;
  }
  return FALSE;
}
