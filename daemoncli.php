<?php

/**
 * @file
 * Main file for service.
 */

// These global variables will be used in all child processes, and might
// influence some Drupal hook execution as well.
global $stop, $debug, $daemon_logging, $daemon_pid, $deamonize; 

$debug          = FALSE; // Global debug mode.
$stop           = FALSE; // Helper for the sig_handler().
$daemon_logging = FALSE; // Should the deamon do file logging?
$daemon_pid     = -1;    // Current process pid.
$deamonize      = TRUE;  // Deamonize.

// Parse command line (getopt emulation).
$args = $_SERVER['argv'];
if (count($args) > 1) {
  foreach ($args as $key => $arg) {
    // Skip first argument, which is command name.
    if ($key) {
      switch ($arg) {
        case "--no-deamon":
          $deamonize = FALSE;
          break;

        case "-d":
        case "--debug":
          $debug = TRUE;
          break;

        case '-l':
        case '--logging':
          $daemon_logging = TRUE;
          break;

        default:
          print "Fatal error: Wrong command line switch '{$arg}'\n";
          return;
      }
    }
  }
}

// Make sure there is not time limit, in most case, this is useless.
set_time_limit(0);

// Check if launched in cli mode.
if (!isset($_SERVER['REMOTE_ADDR'])) {
  $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
}

// Check for posix and pcntl_*() functions.
if (function_exists("pcntl_fork") !== true) {
  print "Fatal error: You must install first php-process package or equivalent\n";
  return;
}

// Required library.
require_once dirname(__FILE__) . '/daemoncli.defines.inc';
require_once dirname(__FILE__) . '/daemoncli.logging.inc';

// Open the log file, this will be share among processes.
daemoncli_log_open_file();

// Launching process to daemonize service. We will do this only if deamon mode
// is asked
if ($deamonize) {
  $pid = pcntl_fork();

  if (!$debug) {
    switch ($pid) {
      // Error.
      case -1:
        break;
  
      // Children.
      case 0:
        daemonize();
        break;
  
      // Parent.
      default:
        $daemon_pid = $pid;
        print "Launching daemon with pid {$pid}\n";
        return;
    }
  }
}

// No deamon mode, run the process.
else {
  daemonize();
}

/**
 * SIG handler.
 * 
 * @param int $signo
 */
function sig_handler($signo) {
  global $stop;

  switch ($signo) {
    case SIGKILL:
    case SIGTERM:
      require_once './includes/bootstrap.inc';      
      drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
      watchdog("daemoncli", "SIGTERM... Waiting end for process", array(), WATCHDOG_INFO);

      // Changing pid number to check if daemon is running
      variable_set(DAEMONCLI_PID, 0);
      variable_set(DAEMONCLI_LAST_EXECUTE, 0);
      $stop = TRUE;
      break;
  }
}

/**
 * Do an initial fork in order to deamonize the process if manually run in
 * a console.
 */
function daemonize() {
  global $stop, $parent, $daemon_pid;
  $first_time = TRUE;

  // Intercept signals.
  declare(ticks = 1);
  pcntl_signal(SIGTERM, "sig_handler");

  while (true) {
    $child_pid = pcntl_fork();

    switch($child_pid) {
      case -1: // Error.
        break;

      case 0: // Child process.
        require_once './includes/bootstrap.inc';
        drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
        require_once dirname(__FILE__) . '/service_children.inc';
        children_process($first_time);
        exit();
        break;

      default: // Parent process.
        $first_time = FALSE;
        pcntl_waitpid($child_pid, $status);
        break;
    }

    // Set pid for debug messages.
    $daemon_pid = $child_pid;

    // Check if sig_handler() caught a stop signal.
    if ($stop === TRUE) {
      // MySQL connection is closed when reaching this code.
      daemoncli_log_message("Demande de sortie du programme\n");
      break;
    }

    usleep(DAEMONCLI_DEFAULT_DELAY);
  }

  return;
}

// Children will never get here.
daemoncli_log_message("Shutdown complete, closing log file\n");
daemoncli_log_open_file(TRUE);

exit(0);
