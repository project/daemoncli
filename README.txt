
CLI Daemon
========== 

To run the daemon you need PHP unix posix_* and process handling extentions.
Using debian based distributions (debian and ubuntu) you don't need to install
anything.

If you are using rpm based distros such as fedora, you need to install the
'php-process' package.

Credits
=======

Thanks to Florent Jousseaume, for original and yet really impressive original
implementation.
